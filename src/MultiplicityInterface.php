<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-multiplicity-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Multiplicity;

/**
 * MultiplicityInterface class file.
 * 
 * This class represent multiplicities. Multiplicities are specified as a doublet
 * of symbols like 0-1, 0-n, 1-1, 1-n, which estimates the number of relations
 * between two models.
 * 
 * @author Anastaszor
 */
interface MultiplicityInterface
{
	
	/**
	 * Gets whether the multiplicity is mandatory. This makes the point between
	 * 0-x and 1-x multiplicities, 1-x are mandatory ones.
	 * 
	 * @return boolean
	 */
	public function isMandatory() : bool;
	
	/**
	 * Gets whether the multiplicity is multiple. This makes the point between
	 * x-1 and x-n multiplicities, x-n are multiple ones.
	 * 
	 * @return boolean
	 */
	public function isMultiple() : bool;
	
	/**
	 * Gets whether two multiplicities are equals.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Gets the multiplicity that is obtained by merging two multiplicities.
	 * This represents the effective relation count between two models without
	 * counting the intermediary model relations.
	 * 
	 * @param MultiplicityInterface $other
	 * @return MultiplicityInterface
	 */
	public function mergeWith(MultiplicityInterface $other) : MultiplicityInterface;
	
}
